#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {

   uint8_t val = 255;
   uint8_t valbk = val;

  printf("0x%lx\n", &val);

  while(1) {
    sleep(1);
    if (val != valbk) {
      printf("value has changed to: %d\nRestoring old value.", val);
      val = valbk;
    }
  }
}
