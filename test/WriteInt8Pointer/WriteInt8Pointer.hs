module Main where

import Foreign
import System.Process
import System.IO
import Unsafe.Coerce

import Data.Memory.Debug

main :: IO ()
main = test 23266 (unsafeCoerce 0x7ffc3d58dfa6)

test :: Pid -> Ptr Word8 -> IO ()
test pid remotePtr =
  do
    putStrLn $ concat ["Pid = ", show pid, ", Ptr = ", show remotePtr]
    -- Remote pointer will be 255, so write something else than that
    res <- processVMWriteV pid remotePtr 1 [123]
    print res
