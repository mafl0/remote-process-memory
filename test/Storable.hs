module Main where

import Foreign
import Data.Memory.Debug



main :: IO ()
main =
  do
    bufPtr <- malloc
    putStr "buf: "
    print bufPtr

    iovPtr <- malloc
    putStr "iov: "
    print iovPtr

    let iov = IOVec bufPtr 1

    putStrLn $ "Poking iov = " ++ show iov
    poke iovPtr iov

    putStrLn "Peeking at iov"
    res <- peek iovPtr
    putStr "Res: "
    print res

    free bufPtr
    free iovPtr

    return ()

