#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>


#define def(t,n,v) \
  t val_##n = v; \
  printf("0x%lx\n", &val_##n);

int main(void) {
  // Primitives
  def(int,Int,-1)
  def(char,Char,-1)
  def(uint8_t,Bool,1)

  // Words
  def(uint8_t,Word8,-1)
  def(uint16_t,Word16,-1)
  def(uint32_t,Word32,-1)
  def(uint64_t,Word64,-1)

  // Int
  def(int8_t,Int8,-1)
  def(int16_t,Int16,-1)
  def(int32_t,Int32,-1)
  def(int64_t,Int64,-1)

  while(1) {
    sleep(1);
  }
}
