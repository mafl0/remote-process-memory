module Main where

main :: IO ()
main = undefined

ptr :: [Ptr Word8]
ptrs =
 [ unsafeCoerce 0x7ffe78b6871c
 , unsafeCoerce 0x7ffe78b68714
 , unsafeCoerce 0x7ffe78b68715
 , unsafeCoerce 0x7ffe78b68716
 , unsafeCoerce 0x7ffe78b68718
 , unsafeCoerce 0x7ffe78b68720
 , unsafeCoerce 0x7ffe78b68728
 , unsafeCoerce 0x7ffe78b68717
 , unsafeCoerce 0x7ffe78b6871a
 , unsafeCoerce 0x7ffe78b68724
 , unsafeCoerce 0x7ffe78b68730
 ]

testInt :: Pid -> Ptr Word8
