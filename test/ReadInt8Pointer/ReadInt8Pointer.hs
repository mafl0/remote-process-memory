module Main where

import Foreign
import System.Process
import System.IO
import Unsafe.Coerce

import Data.Memory.Debug

main :: IO ()
main = test pid ptr

test :: Pid -> Ptr Word8 -> IO ()
test pid remotePtr =
  do
    putStrLn $ concat ["Pid = ", show pid, ", Ptr = ", show remotePtr]
    res <- processVMReadV pid remotePtr 1
    print res
