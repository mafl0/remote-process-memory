#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {

   uint8_t val[10];

   for (uint8_t i = 0; i < 10; ++i) {
      val[i] = i;
   }

  printf("0x%lx\n", &val);
  while(1) {
    sleep(1);
  }
}
