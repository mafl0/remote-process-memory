module Main where

import Foreign
import System.Process
import System.IO
import Unsafe.Coerce

import Data.Memory.Debug

main :: IO ()
main = test 22579 (unsafeCoerce 0x7ffd572e597e)

test :: Pid -> Ptr Word8 -> IO ()
test pid remotePtr =
  do
    putStrLn $ concat ["Pid = ", show pid, ", Ptr = ", show remotePtr]
    res <- processVMReadV pid remotePtr 10
    print res
