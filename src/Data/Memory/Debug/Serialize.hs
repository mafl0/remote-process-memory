module Data.Memory.Debug.Serialize where

import qualified Data.Binary          as B
import qualified Data.ByteString.Lazy as BS
import           Data.ByteString.Lazy (ByteString)
import           Data.Word

-- | Wrapper function for Data.Binary.encode. Our package works with lists of
-- Word8 so this is a convenient helper function that does the conversion for
-- you.
--
-- It is not efficient, but this module is not designed for large amounts of
-- memory
-- transfers anyway.
encode :: B.Binary a => a -> [Word8]
encode = BS.unpack . B.encode

decode :: B.Binary a => [Word8] -> a
decode = B.decode . BS.pack
