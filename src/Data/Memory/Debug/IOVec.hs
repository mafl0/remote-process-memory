{-# LANGUAGE ForeignFunctionInterface #-}

module Data.Memory.Debug.IOVec where

import           Foreign
import           Foreign.C
import           System.Process

data IOVec = IOVec
  { iov_base :: Ptr Word8
  , iov_len  :: Int
  } deriving Show

instance Storable IOVec where
  sizeOf    ~(IOVec base len) = sizeOf base + sizeOf len
  alignment                   = sizeOf

  poke ptr ~(IOVec base len) =
    do
      poke        (castPtr ptr)               base
      pokeByteOff (castPtr ptr) (sizeOf base) len
      return ()

  peek ptr     =
    do
      base <- peek        (castPtr ptr)               :: IO (Ptr Word8)
      len  <- peekByteOff (castPtr ptr) (sizeOf base) :: IO Int

      return (IOVec base len)

