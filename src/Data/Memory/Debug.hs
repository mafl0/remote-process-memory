module Data.Memory.Debug
  ( IOVec(..)

  -- * Reading and writing
  , processVMReadV
  , processVMWriteV

  -- Encoding custom data
  , encode
  , decode

  , module Foreign
  ) where

import Foreign

import Data.Memory.Debug.FFI
import Data.Memory.Debug.IOVec
import Data.Memory.Debug.Serialize

